#-*- coding: utf-8 -*-
import socket
import os
from urllib.parse import urlparse
from datetime import datetime, timedelta
import traceback

from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters,
                          RegexHandler, CallbackContext)
import logging

import sys
import os
from datetime import datetime

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings

from graber.models import Post

class Command(BaseCommand):
    help = """Parse imgur"""

    def handle(self, *args, **options):

        logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                            level=logging.INFO)

        logger = logging.getLogger(__name__)
        def help(update, context):
            update.message.reply_text('Yellow')

        def error(update, context):
            logger.warning('Update "%s" caused error "%s"', update, context.error)            

        def send_video_group(bot, post):
            if post.video_type == 0:
                message = bot.send_video(
                    chat_id=settings.CHAT_NAME,
                    timeout=settings.TIMEOUT,
                    video=post.file_id
                )
            else:
                message = bot.send_animation(
                    chat_id=settings.CHAT_NAME,
                    timeout=settings.TIMEOUT,
                    animation=post.file_id
                )
                
            post.is_posted_group = True
            post.save()
                        
        def send_video(bot, chat_id):
            for p in Post.objects.filter(is_posted_tg=False, is_failed_tg=False,
                                         size__lt=50000000).order_by('-id')[:1]:
                try:
                    message = bot.send_video(timeout=settings.TIMEOUT, chat_id=chat_id,
                                        video=open("%s%s" % (settings.MEDIA_ROOT, p.video), 'rb'))

                    bot.send_message(chat_id=chat_id, text="/send_%s" % p.pk)
                except socket.timeout:
                    print ("Timeout while sending video")
                    p.is_failed_tg = True
                except:
                    p.is_failed_tg = True
                    print ("Error while sending file...")
                    print(traceback.format_exc())
                    print("%s - %s" % (p, p.id))
                else:
                    print("Posted!")
                    p.is_posted_tg = True
                    if message.video:
                        p.file_id = message.video.file_id
                        p.video_type = 0
                    if message.animation:
                        p.video_type = 1
                        p.file_id = message.animation.file_id
                        
                p.save()

        def main_job(context: CallbackContext):
            #send_video(bot)
            print(".", end='')
            post_list = Post.objects.filter(
                    moderated=True, is_failed_tg=False, is_posted_group=False,
                    publication_date__lt=datetime.now())
            #print(post_list.query)
            for p in post_list:
                    send_video_group(context.bot, p)

        def post_now(update, context):
            send_video(context.bot, update.message.chat_id)

        def post_group(update, context):
            post = Post.objects.get(pk=int(context.match[1]))
            try:
                last = Post.objects.filter(moderated=True).order_by('-publication_date')[0]
            except IndexError:
                last_date = datetime.now()
            else:
                if not last.publication_date:
                    last_date = datetime.now()
                else:
                    last_date = last.publication_date
            if last_date < datetime.now():
                last_date = datetime.now()
            print("Last post: %s" % last_date)
            post.publication_date = last_date + timedelta(minutes=settings.POST_INTERVAL)
            post.moderated = True
            post.save()
            update.message.reply_text(
                "Видео #%s будет опубликовано: %s" % (post.pk, post.publication_date))
            
            #send_video_group(bot, post, settings.CHAT_NAME)
            
        updater = Updater(settings.TOKEN, use_context=True)
        jq = updater.job_queue
        d_job = jq.run_repeating(main_job, interval=180, first=0)

        dp = updater.dispatcher
        dp.add_handler(CommandHandler("start", help))
        dp.add_handler(CommandHandler("help", help))
        dp.add_handler(CommandHandler("post", post_now))
        dp.add_handler(MessageHandler(Filters.regex(r"/send_(?P<v_id>\d+)$"),
                                      post_group))
        dp.add_error_handler(error)
        updater.start_polling()
        updater.idle()

