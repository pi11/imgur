#-*- coding: utf-8 -*-
import sys
import requests
import os
from datetime import datetime

from pyquery import PyQuery as pq

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from django.db.models import Q

from graber.models import Post

class Command(BaseCommand):
    help = """Parse imgur"""

    def add_arguments(self, parser):
        parser.add_argument('start_page', type=int, default=0)
        parser.add_argument('end_page', type=int, default=1)
    
    def handle(self, *args, **options):
        base_url = "https://imgur.com/gallery/hot/viral/page/%s/hit?scrolled&set="
        n = datetime.now()
        headers = {"user-agent":"Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)"}


        # first clear old files

        
        for p in Post.objects.filter(
                Q(is_failed_tg=True)|
                Q(is_posted_tg=True)|Q(size__gte=50000000))\
                .exclude(is_file_deleted=True):
            print("Deleting: %s" % p)
            f = "%s%s" % (settings.MEDIA_ROOT, p.video)
            try:
                os.remove(f)
            except:
                pass
            p.is_file_deleted = True
            p.save()
        print("Parsing...")
        
        with requests.Session() as s:
            for p in range(options['start_page'], options['end_page']):
                url = base_url % p
                print ("Parsing: %s" % url)
                r = s.get(url, headers=headers)
                parsed = pq(r.text).make_links_absolute(base_url='https://imgur.com/')
                for l in parsed('.image-list-link').items():
                    p_link = l.attr('href')
                    print ("Parsing: ", p_link)
                    page = s.get(p_link, headers=headers)
                    parsed_page = pq(page.text)
                    video = parsed_page('meta[property="og:video"]').attr('content')
                    if video:
                        print (video)
                        title = parsed_page('h1.post-title').text()
                        print (title)
                        data = s.get(video, headers=headers)
                        new_post = False
                        try:
                            post = Post.objects.get(url=p_link)
                        except Post.DoesNotExist:
                            new_post = True
                            post = Post(url=p_link, title=title)
                            post.save()
                        else:
                            if not post.video: # video is not downloaded
                                new_post = True
                        if new_post:
                            video_path_root = "%s/%s/%s/" % (
                                settings.MEDIA_ROOT, n.year, n.month)
                            try:
                                os.makedirs(video_path_root)
                            except FileExistsError:
                                pass
                            video_path_full = "%s%s.mp4" % (video_path_root, post.pk)
                            with open(video_path_full, "wb+") as f:
                                f.write(data.content)
                            post.size = sys.getsizeof(data.content)
                            post.video = video_path_full.replace(settings.MEDIA_ROOT, '')
                            post.save(update_fields=['video', 'size'])
                        else:
                            print ("Video already loaded, skiping..")

                            
