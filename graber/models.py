from django.db import models

# Create your models here.

    
class Post(models.Model):
    title = models.CharField(max_length=250, null=True, blank=True)
    url = models.CharField(max_length=150, unique=True)
    video = models.CharField(max_length=100, null=True, blank=True)
    size = models.IntegerField(default=0)
    added = models.DateTimeField(auto_now_add=True)
    is_posted_tg = models.BooleanField(default=False)
    is_failed_tg = models.BooleanField(default=False)
    is_posted_tw = models.BooleanField(default=False)
    is_posted_group = models.BooleanField(default=False)
    is_file_deleted = models.BooleanField(default=False)
    file_id = models.CharField(null=True, blank=True, max_length=550)
    publication_date = models.DateTimeField(null=True, blank=True)

    video_type = models.IntegerField(default=0) # 0 - video, 1- animation
    moderated = models.BooleanField(default=False)

    source = models.IntegerField(default=0) # 0 - imgur, 1 - coub
    
    def __str__(self):
        return self.url

    
class Audio(models.Model):
    post = models.ForeignKey(Post)
    audio_file = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return str(self.post)
    
